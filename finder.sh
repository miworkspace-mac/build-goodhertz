#!/bin/bash

NEWLOC=`curl -L https://goodhertz.co/downloads/  2>/dev/null | /usr/local/bin/htmlq -a href a | grep .pkg | head -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi