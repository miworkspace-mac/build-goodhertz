#!/bin/bash -ex

# CONFIG
prefix="Goodhertz"
suffix=""
munki_package_name="Goodhertz"
display_name="Goodhertz"
icon_name=""
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.pkg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
#3.6.4 version
#/usr/sbin/pkgutil --expand-full "app.pkg" pkg

#3.7.4 version -- pkgutil did not work
mkdir pkg
(cd pkg ; xar -xvf ../app.pkg ; cd ..)
mkdir -p build-root/Library
mkdir -p build-root/Library/Audio/Plug-Ins/Components
mkdir -p build-root/Library/Audio/Plug-Ins/VST

# 3.6.4 version:
#(cd build-root; cd Library; pax -rz -f ../..pkg/CanOpener*AU.pkg/Payload)
#(cd build-root; cd Library; pax -rz -f ../../pkg/Lossy*VST.pkg/Payload)
#(cd build-root; cd Library; pax -rz -f ../../pkg/Wow_Control*VST.pkg/Payload)

(cd build-root; cd Library/Audio/Plug-Ins/Components; pax -rz -f ../../../../../pkg/GhzCanOpener*component.pkg/Payload)
(cd build-root; cd Library/Audio/Plug-Ins/VST; pax -rz -f ../../../../../pkg/GhzLossy*.vst.pkg/Payload)
(cd build-root; cd Library/Audio/Plug-Ins/VST; pax -rz -f ../../../../../pkg/GhzWowControl*.vst.pkg/Payload)

key_sound=`find build-root -name '*.component'`
# (cd build-root; pax -rz -f ../pkg/*/Payload)

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$key_sound"/Contents/Info.plist`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`

# hdiutil detach "${mountpoint}"


## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.vst' -or -name '*component' -maxdepth 6 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg "${key_files}" --preinstall_script preinstall.sh | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
#/usr/local/munki/makepkginfo app.pkg > app.plist

plist=`pwd`/app.plist

# Obtain version info
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.15.0"
#defaults write "${plist}" minimum_os_version "${minver}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
#defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist